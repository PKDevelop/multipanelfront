import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from  '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class PostService {
  
  constructor(private http: HttpClient) {}

  getListPostBody = {
    page: 1,
    limit: 10,
    searchName: null
  };

  createPost(endpoint: string, data: any): Observable<any> {
    let token:any = localStorage.getItem('token');
    let tokenToUse = token ?? '';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'POST',
        'X-AUTH-TOKEN': tokenToUse
      }),
      withCredentials: false,
    };
    return this.http.post<any>(environment.apiUrl+endpoint, data, httpOptions);
  }

  patch(endpoint: string, data: any): Observable<any> {
    let token:any = localStorage.getItem('token');
    let tokenToUse = token ?? '';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'PATCH',
        'X-AUTH-TOKEN': tokenToUse
      }),
      withCredentials: false,
    };
    return this.http.patch<any>(environment.apiUrl+endpoint, data, httpOptions);
  }

  put(endpoint: string, data: any): Observable<any> {
    let token:any = localStorage.getItem('token');
    let tokenToUse = token ?? '';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'PUT',
        'X-AUTH-TOKEN': tokenToUse
      }),
      withCredentials: false,
    };
    return this.http.put<any>(environment.apiUrl+endpoint, data, httpOptions);
  }

  delete(endpoint: string, data: any): Observable<any> {
    let token:any = localStorage.getItem('token');
    let tokenToUse = token ?? '';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Methods': 'DELETE',
        'X-AUTH-TOKEN': tokenToUse
      }),
      withCredentials: false,
    };
    return this.http.delete<any>(environment.apiUrl+endpoint, httpOptions);
  }
}
