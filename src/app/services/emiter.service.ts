import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EmiterService {
  public interfaveEvent: EventEmitter<any> = new EventEmitter();

  constructor() {}

  emitEvent(data: any) {
    this.interfaveEvent.emit(data);
  }
}