import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { UsersComponent } from './components/dashboard/users/users.component';
import { NavbarComponent } from './components/dashboard/includes/navbar/navbar.component';
import { FooterComponent } from './components/dashboard/includes/footer/footer.component';
import { PageHeadComponent } from './components/dashboard/includes/page-head/page-head.component'
import { StoreModule } from '@ngrx/store';
import { DataTablesModule } from "angular-datatables";
import { EditComponent } from './components/dashboard/users/edit/edit.component';
import { PaginationComponent } from './components/dashboard/includes/pagination/pagination.component';
import { FiltersComponent } from './components/dashboard/includes/filters/filters.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AddComponent } from './components/dashboard/users/add/add.component';
import { ModalComponent } from './components/dashboard/includes/modal/modal.component';
import { ProjectsComponent } from './components/dashboard/projects/projects.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    UsersComponent,
    NavbarComponent,
    FooterComponent,
    PageHeadComponent,
    EditComponent,
    PaginationComponent,
    FiltersComponent,
    AddComponent,
    ModalComponent,
    ProjectsComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
	  ToastrModule.forRoot(),
    StoreModule.forRoot({}, {}),
    DataTablesModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, './assets/i18n/', '.json');
        },
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
