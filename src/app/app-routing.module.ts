import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
// USERS
import { UsersComponent} from './components/dashboard/users/users.component';
import { EditComponent  as UserDetails } from './components/dashboard/users/edit/edit.component';
import { AddComponent  as CreateUser } from './components/dashboard/users/add/add.component';
// PROJECTS
import { ProjectsComponent} from './components/dashboard/projects/projects.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'users', component: UsersComponent },
  { path: 'users/:id', component: UserDetails },
  { path: 'add-user', component: CreateUser },
  { path: 'projects', component: ProjectsComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
