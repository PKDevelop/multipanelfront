import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from './../../services/post.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {

  public spiner:boolean = false;

  constructor(
    private translate: TranslateService,
    private router: Router,
    private postService: PostService,
    private toastr: ToastrService
    ) {
      translate.setDefaultLang('pl');
    }
  
  authData = {
    email: '',
    password: ''
  };

  ngOnInit(){
      localStorage.clear();
  }

  onSubmit() {
    this.spiner = true;
    this.postService.createPost('/login', this.authData).subscribe(
      (response) => {
        if(response.user != undefined){
          localStorage.setItem('user', JSON.stringify(response.user));
          localStorage.setItem('token', response.user.token);
          this.router.navigate(['']);
          // this.toastr.success("Pomyślnie zalogowano.");
        }
      },
      (error) => {
        this.spiner = false;
        this.toastr.error("Błąd logowania.");
      }
    );
  }
}
