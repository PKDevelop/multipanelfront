import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from './../../../../services/post.service';
import { EmiterService } from './../../../../services/emiter.service';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  
  userForm: FormGroup = this.fb.group({
    id: [null],
    firstName: ['', Validators.required],
    lastName: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: [''],
    position: [''],
    addressLine: [''],
    addressPostCode: [''],
    addressCity: [''],
    addressCountry: ['']
  });
  spiner:boolean = false;
  user: any = null;
  pageTitle :string = 'Profil użytkownika: ';
  breadcrumbs = [{
    title: 'Home',
    link: ''
  },{
    title: 'Użytkownicy',
    link: '/users'
  },{
    title: null
  }];

  constructor(
    private translate: TranslateService,
    private router: Router,
    private postService: PostService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private emiterService: EmiterService
  ) {
    translate.setDefaultLang('pl');
  }

  ngOnInit(): void {
    this.spiner = true;
    this.route.paramMap.subscribe(params => {
      this.getUser(params.get('id'));
    });
  }

  
  onSubmit() {
    this.spiner = true;
    this.postService.patch('/user', this.userForm.value).subscribe(
      (response) => {
        this.getUser(this.user.id);
      },
      (error) => {
        if (error.status === 401) {
          this.router.navigate(['login']);
        }
      }
    );
  }

  getUser(id:any){
    this.postService.createPost('/user/'+id, null).subscribe(
      (response) => {
        this.pageTitle = 'Profil użytkownika: '+response.user.firstName+' '+response.user.lastName;
        this.breadcrumbs[2].title = response.user.firstName+' '+response.user.lastName;
        this.user = response.user;
        this.userForm.setValue(response.user);
        this.spiner = false;
      },
      (error) => {
        if (error.status === 401) {
          this.router.navigate(['login']);
        }
      }
    );
  }
}
