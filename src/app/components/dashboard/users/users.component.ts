import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from './../../../services/post.service';
import { EmiterService } from '../../../services/emiter.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {

  constructor(
    private router: Router,
    private postService: PostService,
    private emiterService: EmiterService,
    private toastr: ToastrService
  ) {
  }

  pageTitle = 'Użytkownicy';
  breadcrumbs = [{
    title: 'Home',
    link: ''
  }, {
    title: 'Użytkownicy',
    link: null
  }];
  spiner: boolean = true;
  postBody = this.postService.getListPostBody;
  usersCollection: any = null;
  lastPage: number = 0;
  removeItem: any = null;
  isModalOpen = false;
  modalTitle = 'Potwierdź';
  modalContent = 'Czy na pewno chcesz usunąć użytkownika.';
  modalItemId: number = 0;

  ngOnInit(): void {
    this.getUsers();
    this.emiterService.interfaveEvent.subscribe((data) => {
      if (data.page !== undefined) {
        this.postBody.page = data.page;
        this.getUsers();
      }
      if (data.limit !== undefined) {
        this.postBody.limit = data.limit;
        this.postBody.page = 1;
        this.getUsers();
      }
      if (data.searchName !== undefined) {
        this.postBody.searchName = data.searchName;
        this.postBody.page = 1;
        this.getUsers();
      }
      if (data.confirmRemove !== undefined) {
        this.postService.delete('/user/' + data.confirmRemove, this.postBody).subscribe(
          (response) => {
            this.closeModal();
            this.spiner = false;
            this.toastr.success(response.message);
            this.getUsers();
          },
          (error) => {
            this.closeModal();
            this.spiner = false;
            console.log(error.message);
          }
        );
      }
    });
  }

  getUsers() {
    this.spiner = true;
    this.postService.createPost('/users', this.postBody).subscribe(
      (response) => {
        this.usersCollection = response.collection;
        this.lastPage = response.pagination.lastPage;
        this.spiner = false;
      },
      (error) => {
        if (error.status === 401) {
          this.router.navigate(['login']);
        }
      }
    );
  }

  openModal(id: number) {
    this.modalItemId = id;
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }

}
