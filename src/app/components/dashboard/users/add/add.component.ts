import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from './../../../../services/post.service';
import { EmiterService } from './../../../../services/emiter.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent {
  userForm: FormGroup = this.fb.group({
    id: [null],
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
    phone: [''],
    position: [''],
    addressLine: [''],
    addressPostCode: [''],
    addressCity: [''],
    addressCountry: ['']
  });
  spiner:boolean = true;
  user: any = null;
  pageTitle :string = 'Dodaj użytkownika: ';
  breadcrumbs = [{
    title: 'Home',
    link: ''
  },{
    title: 'Użytkownicy',
    link: '/users'
  },{
    title: 'Dodaj'
  }];

  constructor(
    private translate: TranslateService,
    private router: Router,
    private postService: PostService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private emiterService: EmiterService,
    private toastr: ToastrService
  ) {
    translate.setDefaultLang('pl');
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.spiner = false;
    }, 200);
    // this.toastr.error('qwdwqdqwdqwd');
  }

  onSubmit() {
    // alert('ok');
    this.spiner = true;
    this.postService.put('/user', this.userForm.value).subscribe(
      (response) => {
        this.spiner = false;
        this.router.navigate(['users']);
        this.toastr.success(response.message);
      },
      (error) => {
        console.log(error);
        this.spiner = false;
        if (error.status === 401) {
          this.router.navigate(['login']);
        }
        this.toastr.error(error.error.message);
      }
    );
  }
}
