import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from './../../services/post.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  
  constructor(
    private router: Router,
    private postService: PostService
  ) { }

  spiner: boolean = true;
  dashboardData: any = [];
  pageTitle = '';
  breadcrumbs = [];

  ngOnInit() {
    this.spiner = true;
    this.postService.createPost('/dashboard', null).subscribe(
      (response) => {
        this.dashboardData.users = response.users;
        this.dashboardData.projects = response.projects;
        this.spiner = false;
      },
      (error) => {
        if (error.status === 401) {
          this.router.navigate(['login']);
        }
      }
    );
   
  }
}
