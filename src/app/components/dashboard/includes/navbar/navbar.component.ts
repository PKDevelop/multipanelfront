import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent{
  
  @Input() spiner:boolean = false;

  constructor(
  ) {
  }

  ngOnInit(): void {
   
  }

}
