import { Component, Input, EventEmitter, Output } from '@angular/core';
import { EmiterService } from './../../../../services/emiter.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent {
  isModalOpen = false;
  @Input() itemId: number = 0;
  @Input() title: string = '';
  @Input() content: string = '';
  @Output() closeModalEvent = new EventEmitter<void>();

  constructor(
    private emiterService: EmiterService
  ) {}

  closeModal() {
    this.closeModalEvent.emit();
  }

  confirm() {
    this.emiterService.emitEvent({ confirmRemove: this.itemId});
  }
}