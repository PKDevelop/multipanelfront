import { Component, Input} from '@angular/core';
import { EmiterService } from './../../../../services/emiter.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent{

  @Input() currentPage: number = 1;
  @Input() currentLimit: number = 10;
  @Input() lastPage: number = 0;

  selectPage: boolean = true;

  constructor(
    private emiterService: EmiterService
  ) {
    if(this.lastPage > 1){
      this.selectPage = false
    }
  }

  setPage(i:number) {
    this.emiterService.emitEvent({ page: i});
  }

  setLimit(i:number) {
    this.emiterService.emitEvent({ limit: i});
  }

  getNumbersArray(max: number): number[] {
    return Array.from({ length: max }, (_, index) => index + 1);
  }

}
