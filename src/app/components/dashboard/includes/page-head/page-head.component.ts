import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-page-head',
  templateUrl: './page-head.component.html',
  styleUrls: ['./page-head.component.css']
})
export class PageHeadComponent {
  @Input() pageTitle: string = 'Example page';
  @Input() breadcrumbs: any;
}
