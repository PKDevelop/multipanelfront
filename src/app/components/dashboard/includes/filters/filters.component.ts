import { Component, Input } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { EmiterService } from './../../../../services/emiter.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent {

  @Input() searchName: any = '';

  private destroy$ = new Subject<void>();

  constructor(
    private emiterService: EmiterService
  ) {}
  
  ngOnInit() {
    const inputElement = document.getElementById('searchName') as HTMLInputElement;

    if (inputElement) {
      fromEvent(inputElement, 'input')
        .pipe(
          debounceTime(1000), // Adjust the time as needed (e.g., 1000ms for 1 second)
          distinctUntilChanged(),
          takeUntil(this.destroy$)
        )
        .subscribe(() => {
          this.emiterService.emitEvent({ searchName: inputElement.value});
        });
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
