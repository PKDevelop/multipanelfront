import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PostService } from './../../../services/post.service';
import { EmiterService } from '../../../services/emiter.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent {

  constructor(
    private router: Router,
    private postService: PostService,
    private emiterService: EmiterService,
    private toastr: ToastrService
  ) {
  }

  pageTitle = 'Projekty';
  breadcrumbs = [{
    title: 'Home',
    link: ''
  }, {
    title: 'Projekty',
    link: null
  }];
  spiner: boolean = true;
  postBody = this.postService.getListPostBody;
  projectsCollection: any = null;
  lastPage: number = 0;
  removeItem: any = null;
  isModalOpen = false;
  modalTitle = 'Potwierdź';
  modalContent = 'Czy na pewno chcesz usunąć projekt.';
  modalItemId: number = 0;

  ngOnInit(): void {
    this.getProjects();
    this.emiterService.interfaveEvent.subscribe((data) => {
      if (data.page !== undefined) {
        this.postBody.page = data.page;
        this.getProjects();
      }
      if (data.limit !== undefined) {
        this.postBody.limit = data.limit;
        this.postBody.page = 1;
        this.getProjects();
      }
      if (data.searchName !== undefined) {
        this.postBody.searchName = data.searchName;
        this.postBody.page = 1;
        this.getProjects();
      }
      if (data.confirmRemove !== undefined) {
        this.postService.delete('/project/' + data.confirmRemove, this.postBody).subscribe(
          (response) => {
            this.closeModal();
            this.spiner = false;
            this.toastr.success(response.message);
            this.getProjects();
          },
          (error) => {
            this.closeModal();
            this.spiner = false;
            console.log(error.message);
          }
        );
      }
    });
  }

  getProjects() {
    this.spiner = true;
    this.postService.createPost('/projects', this.postBody).subscribe(
      (response) => {
        this.projectsCollection = response.collection;
        this.lastPage = response.pagination.lastPage;
        this.spiner = false;
      },
      (error) => {
        if (error.status === 401) {
          this.router.navigate(['login']);
        }
      }
    );
  }

  openModal(id: number) {
    this.modalItemId = id;
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }

}
